'use strict';

const request = require('request-promise-native');

const { diffString } = require("json-diff");

async function compareResponse(host1,host2,path) {

	const url1 = `${host1}${path}`;
	const url2 = `${host2}${path}`;

	console.log(url1);
	console.log(url2);
	try {
		const res1 = JSON.parse(await request(url1)) ;
		const res2 = JSON.parse(await request(url2));
		const  diff = diffString(res1,res2);
		console.log(diff);
	} catch(err) {
		console.error(err.message);
	}
};

async function checkEndpointResponses(host1,host2) {

	await compareResponse(host1,host2,"/v3.1/all");

	await compareResponse(host1,host2,"/v3.1/update");

	let address = "3848";

	await compareResponse(host1,host2,`/v3.1/geocode?address=${address}`);

	address = "3848DB";
	await compareResponse(host1,host2,`/v3.1/geocode?address=${address}`);

	address = "3848db";
	await compareResponse(host1,host2,`/v3.1/geocode?address=${address}`);

	address = "harderwijk";
	await compareResponse(host1,host2,`/v3.1/geocode?address=${address}`);

	let lat = 52.0783365;
	let lng = 5.1184078;

	await compareResponse(host1,host2,`/v3.1/reverse_geocode?lat=${lat}&lng=${lng}`);

	let rad = 50;
	lat = 52.0783365;
	lng = 5.1184078;

	await compareResponse(host1,host2,`/v3.1/shopgps?lat=${lat}&lng=${lng}&rad=${rad}`);
}


async function run() {

	await checkEndpointResponses(
		"http://api.reclamefolder.nl",
		"https://rf-app-api-dot-reclamefoldernl-eu.appspot.com"
	);
}

run();
